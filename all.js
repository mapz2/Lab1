var TokenType;
(function (TokenType) {
    TokenType[TokenType["StringLit"] = 0] = "StringLit";
    TokenType[TokenType["NumberLit"] = 1] = "NumberLit";
    TokenType[TokenType["BoolLit"] = 2] = "BoolLit";
    TokenType[TokenType["String"] = 3] = "String";
    TokenType[TokenType["Number"] = 4] = "Number";
    TokenType[TokenType["Bool"] = 5] = "Bool";
    TokenType[TokenType["If"] = 6] = "If";
    TokenType[TokenType["Else"] = 7] = "Else";
    TokenType[TokenType["While"] = 8] = "While";
    TokenType[TokenType["Semicolon"] = 9] = "Semicolon";
    TokenType[TokenType["Comma"] = 10] = "Comma";
    TokenType[TokenType["LPareth"] = 11] = "LPareth";
    TokenType[TokenType["RPareth"] = 12] = "RPareth";
    TokenType[TokenType["LBrace"] = 13] = "LBrace";
    TokenType[TokenType["RBrace"] = 14] = "RBrace";
    TokenType[TokenType["Assign"] = 15] = "Assign";
    TokenType[TokenType["Plus"] = 16] = "Plus";
    TokenType[TokenType["Minus"] = 17] = "Minus";
    TokenType[TokenType["Multiply"] = 18] = "Multiply";
    TokenType[TokenType["Divide"] = 19] = "Divide";
    TokenType[TokenType["Greater"] = 20] = "Greater";
    TokenType[TokenType["Less"] = 21] = "Less";
    TokenType[TokenType["Equal"] = 22] = "Equal";
    TokenType[TokenType["GreaterEqual"] = 23] = "GreaterEqual";
    TokenType[TokenType["LessEqual"] = 24] = "LessEqual";
    TokenType[TokenType["NotEqual"] = 25] = "NotEqual";
    TokenType[TokenType["Print"] = 26] = "Print";
    TokenType[TokenType["FindLast"] = 28] = "FindLast";
    TokenType[TokenType["FindFirst"] = 27] = "FindFirst";
    TokenType[TokenType["Identifier"] = 29] = "Identifier";
    TokenType[TokenType["Undefined"] = 30] = "Undefined";
})(TokenType || (TokenType = {}));
;

const TOKEN_SYMBOL = {
    [TokenType["Print"]] : "print", 
    [TokenType["If"]] : "if", 
    [TokenType["Else"]] : "else", 
    [TokenType["While"]] : "while", 
    [TokenType["Semicolon"]] : ";", 
    [TokenType["Comma"]] : ",", 
    [TokenType["RPareth"]] : ")", 
    [TokenType["LBrace"]] : "{", 
    [TokenType["RBrace"]] : "}", 
    [TokenType["LPareth"]] : "(", 
    [TokenType["Assign"]] : "=", 
    [TokenType["Plus"]] : "+", 
    [TokenType["Minus"]] : "-", 
    [TokenType["Multiply"]] : "*", 
    [TokenType["Divide"]] : "/", 
    [TokenType["Greater"]] : ">", 
    [TokenType["Less"]] : "<", 
    [TokenType["Equal"]] : "==", 
    [TokenType["GreaterEqual"]] : ">=", 
    [TokenType["LessEqual"]] : "<=", 
    [TokenType["NotEqual"]] : "!=" 
    
};


class Token {
    constructor(type = TokenType.Undefined, data = '') {
        this.type = type;
        this.data = data;
    }
    isOperator() {
        switch (this.type) {
            case TokenType.Assign:
            case TokenType.Plus:
            case TokenType.Minus:
            case TokenType.Multiply:
            case TokenType.Divide:
            case TokenType.Greater:
            case TokenType.Less:
            case TokenType.Equal:
            case TokenType.GreaterEqual:
            case TokenType.LessEqual:
            case TokenType.NotEqual:
            case TokenType.Comma:
                return true;
            default:
                return false;
        }
    }
    isLit() {
        switch (this.type) {
            case TokenType.StringLit:
            case TokenType.NumberLit:
            case TokenType.BoolLit:
                return true;
            default:
                return false;
        }
    }
    isOperand() {
        switch (this.type) {
            case TokenType.Identifier:
            case TokenType.StringLit:
            case TokenType.NumberLit:
            case TokenType.BoolLit:
                return true;
            default:
                return false;
        }
    }
    isFunction() {
        switch (this.type) {
            case TokenType.Print:
            case TokenType.FindFirst:
            case TokenType.FindLast:
                return true;
            default:
                return false;
        }
    }
    isLeftAssociative() {
        if (!this.isOperator()) {
            throw 'not an operator';
        }
        switch (this.type) {
            case TokenType.Plus:
            case TokenType.Minus:
            case TokenType.Multiply:
            case TokenType.Divide:
            case TokenType.Equal:
            case TokenType.NotEqual:
            case TokenType.Comma:
                return true;
            default:
                return false;
        }
    }
    isRightAssociative() {
        if (!this.isOperator()) {
            throw 'not an operator';
        }
        switch (this.type) {
            case TokenType.Assign:
                return true;
            default:
                return false;
        }
    }
    getOperatorPriority() {
        switch (this.type) {
            case TokenType.Multiply:
            case TokenType.Divide:
                return 14;
            case TokenType.Plus:
            case TokenType.Minus:
                return 13;
            case TokenType.Greater:
            case TokenType.Less:
            case TokenType.GreaterEqual:
            case TokenType.LessEqual:
                return 11;
            case TokenType.Equal:
            case TokenType.NotEqual:
                return 10;
            case TokenType.Assign:
                return 2;
            case TokenType.Comma:
                return 1;
            default:
                throw 'not an operator';
        }
    }
    isVarDecl() {
        switch (this.type) {
            case TokenType.String:
            case TokenType.Number:
            case TokenType.Bool:
                return true;
            default:
                return false;
        }
    }
    static varTypeToLit(type) {
        switch (type) {
            case TokenType.String:
                return TokenType.StringLit;
            case TokenType.Number:
                return TokenType.NumberLit;
            case TokenType.Bool:
                return TokenType.BoolLit;
            default:
                throw 'var creation exprected';
        }
    }
    getString() {
        if (!this.isLit()) {
            throw 'cannot convert to string';
        }
        if (this.type == TokenType.StringLit) {
            if (this.data.charAt(0) == '"')
                return this.data.slice(1, this.data.length - 1);
        }
        return this.data;
    }
    toNumber() {
        if (!this.isLit()) {
            throw 'cannot convert to num';
        }
        switch (this.type) {
            case TokenType.StringLit:
            case TokenType.NumberLit:
                return Number(this.data);
            case TokenType.BoolLit:
                switch (this.data) {
                    case 'true': return 1;
                    case 'false': return 0;
                    default: throw 'cannot convert bool';
                }
            default:
                throw 'bad type';
        }
    }
    toBool() {
        if (!this.isLit()) {
            throw 'cannot get bool';
        }
        switch (this.type) {
            case TokenType.StringLit:
                return this.data.length != 0;
            case TokenType.NumberLit:
                return this.toNumber() != 0;
            case TokenType.BoolLit:
                switch (this.data) {
                    case 'true': return true;
                    case 'false': return false;
                    default: throw 'cannot convert bool';
                }
            default:
                throw 'bad type';
        }
    }
    static getDefaultString() {
        return new Token(TokenType.StringLit, '');
    }
    static getDefaultNumber() {
        return new Token(TokenType.NumberLit, '0');
    }
    static getDefaultBool() {
        return new Token(TokenType.BoolLit, 'false');
    }
}
class TokenMatch {
    constructor(matched = false, type = TokenType.Undefined, data = '') {
        this.matched = matched;
        this.type = type;
        this.data = data;
    }
}
class TokenDef {
    constructor(type = TokenType.Undefined, regex = RegExp('')) {
        this.type = type;
        this.regex = regex;
    }
    match(temp) {
        const res = this.regex.exec(temp);
        if (res != null) {
            return new TokenMatch(true, this.type, res[0]);
        }
        else {
            return new TokenMatch();
        }
    }
}
const TokenDefs = [
    new TokenDef(TokenType.StringLit, /^"[^"]*"/),
    new TokenDef(TokenType.NumberLit, /^[-+]?[0-9]*\.?[0-9]+/),
    new TokenDef(TokenType.BoolLit, /^true(?=\W)|^false(?=\W)/),
    new TokenDef(TokenType.String, /^string(?=\b)/),
    new TokenDef(TokenType.Number, /^num(?=\b)/),
    new TokenDef(TokenType.Bool, /^bool(?=\b)/),
    new TokenDef(TokenType.If, /^if(?=\b)/),
    new TokenDef(TokenType.Else, /^else(?=\b)/),
    new TokenDef(TokenType.While, /^while(?=\b)/),
    new TokenDef(TokenType.Semicolon, /^;/),
    new TokenDef(TokenType.Comma, /^,/),
    new TokenDef(TokenType.LPareth, /^\(/),
    new TokenDef(TokenType.RPareth, /^\)/),
    new TokenDef(TokenType.LBrace, /^{/),
    new TokenDef(TokenType.RBrace, /^}/),
    new TokenDef(TokenType.Assign, /^=/),
    new TokenDef(TokenType.Plus, /^\+/),
    new TokenDef(TokenType.Minus, /^-/),
    new TokenDef(TokenType.Multiply, /^\*/),
    new TokenDef(TokenType.Divide, /^[/]/),
    new TokenDef(TokenType.Greater, /^>(?!=)/),
    new TokenDef(TokenType.Less, /^<(?!=)/),
    new TokenDef(TokenType.Equal, /^==/),
    new TokenDef(TokenType.GreaterEqual, /^>=/),
    new TokenDef(TokenType.LessEqual, /^<=/),
    new TokenDef(TokenType.NotEqual, /^!=/),
    new TokenDef(TokenType.Print, /^print/),
    new TokenDef(TokenType.FindLast, /^findLast/),
    new TokenDef(TokenType.FindFirst, /^findFirst/),
    new TokenDef(TokenType.Identifier, /^[A-Za-z_]\w*/)
];
class Tokenizer {
    constructor(tokenDefs = []) {
        this.tokenDefs = tokenDefs;
    }
    tokenize(text) {
        const tokens = [];
        while (text != '') {
            const match = this.findMatch(text);
            if (match.matched) {
                
                tokens.push(new Token(match.type, match.data));
                text = text.substr(match.data.length);
            }
            else {
                if (this.isWhiteSpace(text)) {
                    text = text.substr(1);
                }
                else {
                    throw 'bad token';
                }
            }
        }
        return tokens;
    }
    findMatch(text) {
        for (const tokenDef of this.tokenDefs) {
            const match = tokenDef.match(text);
            if (match.matched) {
                return match;
            }
        }
        return new TokenMatch();
    }
    isWhiteSpace(text) {
        return /^\s/.test(text);
    }
}
class Identifier extends Token {
    constructor(name, token) {
        super(token.type, token.data);
        this.name = name;
    }
}
class Expression {
    constructor(tokens = [], identifiers = [[]]) {
        this.identifiers = identifiers;
        this.outputStream = '';
        for (let i = 1; i < tokens.length; ++i) {
            if ((tokens[i - 1].type == TokenType.NumberLit || tokens[i - 1].type == TokenType.Identifier) && tokens[i].type == TokenType.NumberLit && (tokens[i].data.charAt(0) == '-' || tokens[i].data.charAt(0) == '+')) {
                tokens.splice(i, 0, new Token(TokenType.Plus, '+'));
            }
        }
        this.expression = this.shuntingYard(tokens);
    }
    getOutput() {
        return this.outputStream;
    }
    shuntingYard(tokens) {
        const operatorStack = [];
        const output = [];
        for (const token of tokens) {
            if (token.isOperand()) {
                output.push(token);
            }
            else if (token.isFunction()) {
                operatorStack.push(token);
            }
            else if (token.isOperator()) {
                while (operatorStack.length > 0 &&
                    (operatorStack[operatorStack.length - 1].isFunction() ||
                        (operatorStack[operatorStack.length - 1].isOperator() &&
                            (operatorStack[operatorStack.length - 1].getOperatorPriority() >
                                token.getOperatorPriority() ||
                                (operatorStack[operatorStack.length - 1].isLeftAssociative() &&
                                    operatorStack[operatorStack.length - 1].getOperatorPriority() ==
                                        token.getOperatorPriority())))) &&
                    operatorStack[operatorStack.length - 1].type != TokenType.LPareth) {
                    output.push(operatorStack.pop());
                }
                operatorStack.push(token);
            }
            else if (token.type == TokenType.LPareth) {
                operatorStack.push(token);
            }
            else if (token.type == TokenType.RPareth) {
                while (operatorStack.length > 0 &&
                    operatorStack[operatorStack.length - 1].type != TokenType.LPareth) {
                    output.push(operatorStack.pop());
                }
                if (operatorStack.length == 0) {
                    throw 'no left pareth';
                }
                else if (operatorStack[operatorStack.length - 1].type == TokenType.LPareth) {
                    operatorStack.pop();
                }
            }
            else {
                throw 'bad expression token';
            }
        }
        while (operatorStack.length != 0) {
            output.push(operatorStack.pop());
        }
        return output;
    }
    process() {
        const res = [];
        for (const token of this.expression) {
            if (token.isOperand()) {
                res.push(token);
            }
            else {
                if (token.isOperator()) {
                    res.push(this.processOperator(token, res));
                }
                else if (token.isFunction()) {
                    res.push(this.processFunction(token, res));
                }
                else {
                    throw 'bad token in expression';
                }
            }
        }
        if (res.length > 1) {
            throw 'bad expression';
        }
        if (res.length == 0) {
            return new Token();
        }
        return res[0];
    }
    getValue(token) {
        switch (token.type) {
            case TokenType.StringLit:
            case TokenType.NumberLit:
            case TokenType.BoolLit:
                return token;
            case TokenType.Identifier:
                for (let i = this.identifiers.length - 1; i >= 0; --i) {
                    for (let j = this.identifiers[i].length - 1; j >= 0; --j) {
                        if (this.identifiers[i][j].name == token.data) {
                            return new Token(this.identifiers[i][j].type, this.identifiers[i][j].data);
                        }
                    }
                }
                throw 'identifier was not declared';
            default:
                throw 'not a value';
        }
    }
    processOperator(op, args) {
        if (!op.isOperator()) {
            throw 'not an operator';
        }
        if (args.length < 2) {
            throw '2 args required';
        }
        const opd2 = args.pop();
        const opd1 = args.pop();
        switch (op.type) {
            case TokenType.Assign:
                this.setValue(opd1, this.getValue(opd2));
                return this.getValue(opd1);
            case TokenType.Plus:
                return this.Add(opd1, opd2);
            case TokenType.Minus:
                return this.Subtract(opd1, opd2);
            case TokenType.Multiply:
                return this.Multiply(opd1, opd2);
            case TokenType.Divide:
                return this.Divide(opd1, opd2);
            case TokenType.Greater:
                return this.Greater(opd1, opd2);
            case TokenType.Less:
                return this.Less(opd1, opd2);
            case TokenType.Equal:
                return this.Equal(opd1, opd2);
            case TokenType.GreaterEqual:
                return this.GreaterEqual(opd1, opd2);
            case TokenType.LessEqual:
                return this.LessEqual(opd1, opd2);
            case TokenType.NotEqual:
                return this.NotEqual(opd1, opd2);
        }
    }
    Add(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (opd1.type == TokenType.StringLit || opd2.type == TokenType.StringLit) {
            return new Token(TokenType.StringLit, opd1.getString() + opd2.getString());
        }
        else if (opd1.type == TokenType.NumberLit || opd2.type == TokenType.NumberLit) {
            return new Token(TokenType.NumberLit, (opd1.toNumber() + opd2.toNumber()).toString());
        }
        else {
            return new Token(TokenType.BoolLit, (opd1.toBool() || opd2.toBool()).toString());
        }
    }
    Subtract(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (opd1.type != TokenType.NumberLit || opd2.type != TokenType.NumberLit) {
            throw 'cant subtact';
        }
        return new Token(TokenType.NumberLit, (opd1.toNumber() - opd2.toNumber()).toString());
    }
    Multiply(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (opd1.type == TokenType.NumberLit && opd2.type == TokenType.NumberLit) {
            return new Token(TokenType.NumberLit, (opd1.toNumber() * opd2.toNumber()).toString());
        }
        else if (opd1.type == TokenType.NumberLit && opd2.type == TokenType.StringLit ||
            opd1.type == TokenType.StringLit && opd2.type == TokenType.NumberLit) {
            let n;
            let s;
            if (opd1.type == TokenType.NumberLit) {
                n = opd1.toNumber();
                s = opd2.getString();
            }
            else {
                n = opd2.toNumber();
                s = opd1.getString();
            }
            let res = '';
            for (let i = 0; i < n; ++i) {
                res += s;
            }
            return new Token(TokenType.StringLit, res);
        }
        throw 'cant multiply';
    }
    Divide(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (opd1.type != TokenType.NumberLit || opd2.type != TokenType.NumberLit) {
            throw 'cant divide';
        }
        return new Token(TokenType.NumberLit, (opd1.toNumber() / opd2.toNumber()).toString());
    }
    Greater(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (!opd1.isLit() || opd1.type != opd2.type) {
            throw 'cant compare';
        }
        switch (opd1.type) {
            case TokenType.StringLit:
                return new Token(TokenType.BoolLit, (opd1.data > opd2.data).toString());
            case TokenType.NumberLit:
                return new Token(TokenType.BoolLit, (opd1.toNumber() > opd2.toNumber()).toString());
            case TokenType.BoolLit:
                return new Token(TokenType.BoolLit, (opd1.toBool() > opd2.toBool()).toString());
            default:
                throw 'bad type';
        }
    }
    Less(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (!opd1.isLit() || opd1.type != opd2.type) {
            throw 'cant compare';
        }
        switch (opd1.type) {
            case TokenType.StringLit:
                return new Token(TokenType.BoolLit, (opd1.data < opd2.data).toString());
            case TokenType.NumberLit:
                return new Token(TokenType.BoolLit, (opd1.toNumber() < opd2.toNumber()).toString());
            case TokenType.BoolLit:
                return new Token(TokenType.BoolLit, (opd1.toBool() < opd2.toBool()).toString());
            default:
                throw 'bad type';
        }
    }
    Equal(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (!opd1.isLit() || opd1.type != opd2.type) {
            throw 'cant compare';
        }
        switch (opd1.type) {
            case TokenType.StringLit:
                return new Token(TokenType.BoolLit, (opd1.data == opd2.data).toString());
            case TokenType.NumberLit:
                return new Token(TokenType.BoolLit, (opd1.toNumber() == opd2.toNumber()).toString());
            case TokenType.BoolLit:
                return new Token(TokenType.BoolLit, (opd1.toBool() == opd2.toBool()).toString());
            default:
                throw 'bad type';
        }
    }
    GreaterEqual(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (!opd1.isLit() || opd1.type != opd2.type) {
            throw 'cant compare';
        }
        switch (opd1.type) {
            case TokenType.StringLit:
                return new Token(TokenType.BoolLit, (opd1.data >= opd2.data).toString());
            case TokenType.NumberLit:
                return new Token(TokenType.BoolLit, (opd1.toNumber() >= opd2.toNumber()).toString());
            case TokenType.BoolLit:
                return new Token(TokenType.BoolLit, (opd1.toBool() >= opd2.toBool()).toString());
            default:
                throw 'bad type';
        }
    }
    LessEqual(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (!opd1.isLit() || opd1.type != opd2.type) {
            throw 'cant compare';
        }
        switch (opd1.type) {
            case TokenType.StringLit:
                return new Token(TokenType.BoolLit, (opd1.data <= opd2.data).toString());
            case TokenType.NumberLit:
                return new Token(TokenType.BoolLit, (opd1.toNumber() <= opd2.toNumber()).toString());
            case TokenType.BoolLit:
                return new Token(TokenType.BoolLit, (opd1.toBool() <= opd2.toBool()).toString());
            default:
                throw 'bad type';
        }
    }
    NotEqual(token1, token2) {
        const opd1 = this.getValue(token1);
        const opd2 = this.getValue(token2);
        if (!opd1.isLit() || opd1.type != opd2.type) {
            throw 'cant compare';
        }
        switch (opd1.type) {
            case TokenType.StringLit:
                return new Token(TokenType.BoolLit, (opd1.data != opd2.data).toString());
            case TokenType.NumberLit:
                return new Token(TokenType.BoolLit, (opd1.toNumber() != opd2.toNumber()).toString());
            case TokenType.BoolLit:
                return new Token(TokenType.BoolLit, (opd1.toBool() != opd2.toBool()).toString());
            default:
                throw 'bad type';
        }
    }
    setValue(id, token) {
        if (id.type != TokenType.Identifier) {
            throw 'id expected';
        }
        for (let i = this.identifiers.length - 1; i >= 0; --i) {
            for (let j = this.identifiers[i].length - 1; j >= 0; --j) {
                if (this.identifiers[i][j].name == id.data) {
                    if (this.identifiers[i][j].type != token.type) {
                        throw 'types conflict';
                    }
                    this.identifiers[i][j].data = token.data;
                    return;
                }
            }
        }   
        console.log();
        
        throw 'undeclared id';
    }
    processFunction(func, args) {
        if (!func.isFunction()) {
            throw 'not a function';
        }
        switch (func.type) {
            case TokenType.Print:
                if (args.length < 1) {
                    throw '2 args required';
                }
                const text = this.getValue(args.pop()).getString();
                this.outputStream += text;
                //console.log(text);
                document.getElementById('output').innerHTML += this.outputStream;
                document.getElementById('output').innerHTML += "\n";
                return new Token(TokenType.NumberLit, text.length.toString());
            case TokenType.FindFirst:
                if (args.length < 2) {
                    throw '2 args required';
                }
                const fullText1 = this.getValue(args.pop()).getString();
                const word1 = this.getValue(args.pop()).getString();
                return new Token(TokenType.NumberLit, fullText1.indexOf(word1).toString());
            case TokenType.FindLast:
                if (args.length < 2) {
                    throw '2 args required';
                }
                const fullText2 = this.getValue(args.pop()).getString();
                const word2 = this.getValue(args.pop()).getString();
                return new Token(TokenType.NumberLit, fullText2.lastIndexOf(word2).toString());
            default:
                throw 'missed function declaration';
        }
    }
}
class Statement {
    constructor(identifiers) {
        this.identifiers = identifiers;
        this.outputStream = '';
    }
    process() { }
    getOutput() {
        return this.outputStream;
    }
}
class SimpleStatement extends Statement {
    constructor(identifiers, expression, currentType) {
        super(identifiers);
        this.expression = expression;
        this.currentType = currentType;
    }
    process() {
        this.expression.process();
        this.outputStream += this.expression.getOutput();
        //this.outputStream = this.expression.getOutput();
    }
}
class NewVariableStatement extends Statement {
    constructor(identifiers, type, name, expression, currentType) {
        super(identifiers);
        this.type = type;
        this.name = name;
        this.expression = expression;
        this.currentType = currentType;
    }
    process() {
        let res = this.expression.process();
        if (res.type == TokenType.Undefined) {
            switch (this.type) {
                case TokenType.StringLit:
                    res = Token.getDefaultString();
                case TokenType.NumberLit:
                    res = Token.getDefaultNumber();
                case TokenType.BoolLit:
                    res = Token.getDefaultBool();
            }
        }
        else if (res.type != this.type) {
            throw 'bad initialization';
        }
        this.outputStream += this.expression.getOutput();
        
        for (let i = this.identifiers[this.identifiers.length - 1].length - 1; i >= 0; --i) {
            if (this.identifiers[this.identifiers.length - 1][i].name == this.name) {
                throw 'not allowed redeclaration';
            }
        }
        this.identifiers[this.identifiers.length - 1].push(new Identifier(this.name, res));
    }
}
class BlockStatement extends Statement {
    constructor(identifiers, statements, currentType) {
        super(identifiers);
        this.statements = statements;
        this.currentType = currentType;
    }
    process() {
        this.identifiers.push([]);
        for (const statement of this.statements) {
            statement.process();
        }
        this.identifiers.pop();
    }
}
class IfStatement extends Statement {
    constructor(identifiers, condition, onConditionTrue, currentType, onConditionFalse = null) {
        super(identifiers);
        this.condition = condition;
        this.onConditionTrue = onConditionTrue;
        this.onConditionFalse = onConditionFalse;
        this.currentType = currentType;
    }
    process() {
        this.identifiers.push([]);
        if (this.condition.process().toBool()) {
            this.onConditionTrue.process();
        }
        else if (this.onConditionFalse != null) {
            this.onConditionFalse.process();
        }
        this.identifiers.pop();
    }
}
class WhileStatement extends Statement {
    constructor(identifiers, condition, body, currentType) {
        super(identifiers);
        this.condition = condition;
        this.body = body;
        this.currentType = currentType;
    }
    process() {
        this.identifiers.push([]);
        while (this.condition.process().toBool()) {
            this.body.process();
        }
        this.identifiers.pop();
    }
}
class Interpretator {
    constructor(tokens) {
        this.tokens = tokens;
        this.identifiers = [[]];
        this.output = '';
        this.pos = 0;
    }
    process() {
        while (this.pos < this.tokens.length) {
            const x = this.readStatement();
            //console.log(x);
            //debugger
            x.process();
        }
    }
    readStatement() {
        switch (this.tokens[this.pos].type) {
            case TokenType.String:
            case TokenType.Number:
            case TokenType.Bool: {
                return this.readNewVariableStatement();
            }
            case TokenType.LBrace:{
                return this.readBlockStatement();
            }
            case TokenType.If:
                return this.readIfStatement();
            case TokenType.While:
                return this.readWhileStatement();
            default:
                return this.readSimpleStatement();
        }
    }
    getToken(type) {


        if (this.pos < this.tokens.length && this.tokens[this.pos].type != type) {
            throw 'another token expected';
        }
    }
    tryToken(type) {

        return this.pos < this.tokens.length && this.tokens[this.pos].type === type;
    }
    readSimpleStatement() {
        const expr = [];
        var currentOperation = [TOKEN_SYMBOL[this.tokens[this.pos].type]];
        var currFunc = TOKEN_SYMBOL[this.tokens[this.pos].type];
        while (this.pos < this.tokens.length && this.tokens[this.pos].type != TokenType.Semicolon) {
            currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos+1].type]);
            //console.log(" " + this.tokens[this.pos].type);
            expr.push(this.tokens[this.pos++]);
        }
        if (currFunc != undefined){
            console.log("|                 |_    " + currFunc + "(string)") ;
        }
      
        if (this.pos == this.tokens.length) {
            throw 'unexpected end';
        }
        ++this.pos;
        return new SimpleStatement(this.identifiers, new Expression(expr, this.identifiers), currFunc);
    }
    readNewVariableStatement() {
        const type = Token.varTypeToLit(this.tokens[this.pos++].type);
        this.getToken(TokenType.Identifier);
        const name = this.tokens[this.pos++].data;
        // if (!this.tryToken(TokenType.Assign)) {
        //     this.getToken(TokenType.Semicolon);
            
        // }
        var currentOperation = TOKEN_SYMBOL[this.tokens[this.pos].type];
        //var currentOperation = [TOKEN_SYMBOL[this.tokens[this.pos].type]];
        ++this.pos;
        const expr = [];
        while (this.pos < this.tokens.length && this.tokens[this.pos].type != TokenType.Semicolon) {
            expr.push(this.tokens[this.pos++]);
        }

        if (this.pos == this.tokens.length) {
            throw 'unexpected end';
        }
        ++this.pos;
        //debugger
        console.log("|    |_  " + currentOperation);
        console.log("|        |_  " + name);
        console.log("|        |_  " + "variable");
        return new NewVariableStatement(this.identifiers, type, name, new Expression(expr, this.identifiers), currentOperation);
    }
    readBlockStatement() {
        this.getToken(TokenType.LBrace);
        var currentOperation = [TOKEN_SYMBOL[this.tokens[this.pos].type]];
        //console.log("|       |_   " + currentOperation + " ... }");
        ++this.pos;
        const statements = [];
        while (!this.tryToken(TokenType.RBrace)) {

            statements.push(this.readStatement());
        }
        ++this.pos;
        return new BlockStatement(this.identifiers, statements, currentOperation );
    }
    readIfStatement() {
        var currentOperation = [TOKEN_SYMBOL[this.tokens[this.pos].type]];
        console.log("|            |_  " + currentOperation);
        this.getToken(TokenType.If);
        ++this.pos;
        this.getToken(TokenType.LPareth);
        currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos].type]);
        ++this.pos;
        const cond = [];
        let pareth = 1;
        while (true) {
            if (this.tokens[this.pos].type == TokenType.LPareth) {
                console.log(currentOperation);
                ++pareth;
            }
            else if (this.tokens[this.pos].type == TokenType.RPareth) {
                if (--pareth == 0) {
                    currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos].type]);
                    ++this.pos;
                    break;
                }
            }
            cond.push(this.tokens[this.pos++]);
            console.log("|                 |_   " + this.tokens[this.pos-1].data);
            
        }
        const onCondTrue = this.readStatement();
        if (this.tryToken(TokenType.Else)) {
            var currForElse = TOKEN_SYMBOL[this.tokens[this.pos].type];
            //currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos].type]);
            //console.log(currentOperation);
            ++this.pos;
            console.log("|            |_  " + currForElse);
            return new IfStatement(this.identifiers, new Expression(cond, this.identifiers), onCondTrue, currentOperation);
        }
        else {
            return new IfStatement(this.identifiers, new Expression(cond, this.identifiers), onCondTrue, currentOperation);
        }
    }
    readWhileStatement() {
        var currentOperation = [TOKEN_SYMBOL[this.tokens[this.pos].type]];
        console.log("|    |_  " + currentOperation);
        this.getToken(TokenType.While);
        ++this.pos;
        this.getToken(TokenType.LPareth);
        //currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos].type]);
        ++this.pos;
        const cond = [];
        let pareth = 1;
        while (true) {
            if (this.tokens[this.pos].type == TokenType.LPareth) {
                console.log(currentOperation);
                //currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos].type]);
                ++pareth;
            }
            else if (this.tokens[this.pos].type == TokenType.RPareth) {
                if (--pareth == 0) {
                    currentOperation.push(TOKEN_SYMBOL[this.tokens[this.pos].type]);
                    ++this.pos;
                    break;
                }
            }
            cond.push(this.tokens[this.pos++]);
            console.log("|       |_   " + this.tokens[this.pos-1].data);
        }
        const body = this.readStatement();
        return new WhileStatement(this.identifiers, new Expression(cond, this.identifiers), body, currentOperation);
    }
}
document.getElementById('run').onclick = () => {
    const tokenizer = new Tokenizer(TokenDefs);
    const tokens = tokenizer.tokenize(document.getElementById('text').value);
    // eval(program);
    const interpret = new Interpretator(tokens);
    interpret.process();
};
